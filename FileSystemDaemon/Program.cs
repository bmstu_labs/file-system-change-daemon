﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;

namespace FileSystemDaemon
{

    enum LoggingType
    {
        ltConsole,
        ltDebug,
        ltFile
    }

    internal class Program
    {
        private static readonly Logger consoleLogger = new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.Debug()
                .CreateLogger();

        private static readonly Logger fileLogger = new LoggerConfiguration()
                .WriteTo.File("FolderChangeLog.log")
                .MinimumLevel.Debug()
                .CreateLogger();

        private static readonly Logger debugLogger = new LoggerConfiguration()
                .WriteTo.Debug()
                .MinimumLevel.Debug()
                .CreateLogger();

        static void Main(string[] args)
        { 
            Console.ForegroundColor = ConsoleColor.Yellow;
            bool FileLogginEnabled = false;
            bool ConsoleLogginEnabled = false;
            bool DebugLogginEnabled = false;

            if (args.Length == 0)
            {
                consoleLogger.Fatal("Programm has to be started with string argument - path to track");
                return;
            }
            if (!Directory.Exists(args[0]))
            {
                consoleLogger.Fatal("Path to track \"{0}\" not found", args[0]);
                return;
            }

            ILoggerFactory factory = new LoggerFactory().AddSerilog(consoleLogger);

            Watcher w = new(args[0], factory.CreateLogger<Program>());

            /*
            _logger.Information( @"Any time press:

  ""f"" - for file logging,
  ""c"" - for console logging
  ""d"" - for debug logging
  ""x"" for exit

Press same key againg to stop logging
");*/

            ConsoleKey key;

            while ((key = Console.ReadKey(true).Key) != ConsoleKey.X)
            {
                switch (key)
                {
                    case ConsoleKey.C:

                        if (ConsoleLogginEnabled = !ConsoleLogginEnabled)
                        {
                            w.Changed += LogToConsole;
                            consoleLogger.Information("Console log on");
                        }
                        else
                        {
                            w.Changed -= LogToConsole;
                            consoleLogger.Information("Console log off");
                        }

                        break;

                    case ConsoleKey.D:

                        if (DebugLogginEnabled = !DebugLogginEnabled)
                        {
                            w.Changed += LogToDebug;
                            consoleLogger.Information("Debug log on");
                        }
                        else
                        {
                            w.Changed -= LogToDebug;
                            consoleLogger.Information("Debug log off");
                        }

                        break;

                    case ConsoleKey.F:

                        if (FileLogginEnabled = !FileLogginEnabled)
                        {
                            w.Changed += LogToFile;
                            consoleLogger.Information("File log on");
                        }
                        else
                        {
                            w.Changed -= LogToFile;
                            consoleLogger.Information("File log off");
                        }
                        
                        break;

                    default: break;
                };
            }
        }

        private static void LogToFile(List<ChangeData> changes)
        {
            foreach (var change in changes)
                fileLogger.Information(change.ToString());
        }

        private static void LogToConsole(List<ChangeData> changes)
        {
            foreach (var change in changes)
                consoleLogger.Information(change.ToString());
        }

        private static void LogToDebug(List<ChangeData> changes)
        {
            foreach (var change in changes)
                debugLogger.Information(change.ToString());
        }
    }
}